function Block(i, j, width, height, offset, bottom){
	this.offset = offset;
	this.x = i * width;
	this.y = j * height + this.offset;
	this.height = height;
	this.width = width;
	this.letters = '0123456789ABCDEF'.split('');
	this.color = '#';
	for (var i = 0; i < 6; i++ ) {
       this.color += this.letters[Math.round(Math.random() * 15)];
	}
	
	this.i = i;
	this.j = j;
	this.hit = false;
	
	this.borders = {T: false, B: bottom, L: false, R: false}
}

Block.prototype.show = function(){
	if(!this.hit){
		
		ctx.fillStyle = this.color; //blank colour
		ctx.strokeStyle = "white";
		ctx.fillRect(this.x, this.y, this.width , this.height);
		ctx.strokeRect(this.x, this.y, this.width , this.height);
	}
}

Block.prototype.setBorders = function(whichBorder){
	switch(whichBorder){
		case 'T': 	this.borders.T = true;
					this.borders.B = false;	
					break;
		case 'B': 	this.borders.T = false;
					this.borders.B = true;
					break;
		case 'L': 	this.borders.L = true; 
					this.borders.R = false;
					break;
		case 'R': 	this.borders.L = false; 
					this.borders.R = true;
					break;
		default: ;
	}
}

Block.prototype.setOffset = function(offset){
	this.offset += offset;
	this.y += offset;
}