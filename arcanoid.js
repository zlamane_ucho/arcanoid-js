var cvs;
var ctx;

const MAX_SPEED = 8;
const ROWS_OF_BLOCKS = 10;
const BLOCK = {height: 20, width:80};
const PADDLE = {height: 15, width:150, posX:0, posY:0};
const FPS = 60;
const GO_DOWN = 10;

var cols;
var counter = 0;
var offset = 0;
var ballMoving = false;
var isLost = false;

var ball = {posX: 400, posY: 400, speedX: 0, speedY: MAX_SPEED, radius: 10};
var Ball = {B: ball.posY - ball.radius, T: ball.posY + ball.radius, L: ball.posX + ball.radius, R: ball.posX - ball.radius};

function calculateMousePos(evt){
	var rect = cvs.getBoundingClientRect();
	var root = document.documentElement;
	var mouseX = evt.clientX - rect.left - root.scrollLeft;
	var mouseY = evt.clientY - rect.top - root.scrollTop;
	return{
		x: mouseX,
		y: mouseY
	};
}

window.onload = function(){
	
	cvs = document.getElementById('gameCanvas');
	ctx = cvs.getContext('2d');
	
	PADDLE.posY = cvs.height - PADDLE.height;
	
	showCanvas();
	initBlocks();
	
	setInterval(function(){
		checkLost();
		if(!isLost){
			if(ballMoving) {
				checkBorders();
				moveBall();
				checkOffsetCounter();
			}
			else {
				checkBorders();
				ball.speedX = 0;
				ball.speedY = -MAX_SPEED;
				ball.posX = PADDLE.posX + PADDLE.width/2;
				ball.posY = cvs.height - PADDLE.height - ball.radius;
			}
			showCanvas();
			showBlocks();
			showPaddle();
			showBall();
		}
		else{
			showCanvas();
			showEndScreen();
		}
			}, 1000/FPS);
				
	cvs.addEventListener('mousemove', function(evt){
		var mousePos = calculateMousePos(evt);
		PADDLE.posX = mousePos.x - PADDLE.width/2;
	});
	
	document.onkeydown = checkKey;
}

function make2DArray(cols, rows) {
	var arr = new Array(cols);
	for(var i = 0; i < arr.length; i++)
	{
		arr[i] = new Array(rows);
	}
	return arr;
}

function initBlocks(){
	cols = Math.floor(cvs.width/BLOCK.width);
	grid = make2DArray(cols, ROWS_OF_BLOCKS);
	for(var i = 0; i < cols; i++){
		for(var j = 0; j < ROWS_OF_BLOCKS; j++)
		{
			grid[i][j] = new Block(i, j, BLOCK.width, BLOCK.height, offset, true);
		}
	}
}

function showBlocks(){
	for(var i = 0; i < cols; i++){
		for(var j = 0; j < ROWS_OF_BLOCKS; j++)
		{
			grid[i][j].show();
		}
	}
}

function showCanvas(){
	ctx.fillStyle = "black";
	ctx.fillRect(0, 0, cvs.width, cvs.height);
}

function showPaddle(){
	ctx.fillStyle = "white";
	ctx.fillRect(PADDLE.posX, PADDLE.posY, PADDLE.width, PADDLE.height);
}

function showBall(){
	ctx.fillStyle = "white";
	ctx.beginPath();
	ctx.arc(ball.posX, ball.posY, ball.radius, 0, Math.PI*2, true);
	ctx.fill();
}

function showEndScreen(){
	ctx.fillStyle = "white";
	ctx.font = "50px Times New Roman";
	var endText = "LOST";
	ctx.fillText(endText, cvs.width/2 - endText.length*12.5, cvs.height/2);
}

function moveBall(){
	switch(checkCollision()){
		case 1: ball.speedX = -ball.speedX; break;
		case 2: ball.speedY = -ball.speedY; break;
		case 66: ballMoving = !ballMoving; break;
		case 4: var angle = checkReflectionAngle();
				ball.speedX = angle.x * MAX_SPEED;
				ball.speedY = -angle.y * MAX_SPEED;
				counter++;
				break;
		default: ;
	}
	ball.posY += ball.speedY;
	ball.posX += ball.speedX;
}

function checkCollision(){
	if(ball.posX - ball.radius <= 0) return 1; //left wall
	else if (ball.posX + ball.radius >= cvs.width) return 1; //right wall
	else if (ball.posY + ball.radius >= cvs.height - PADDLE.height &&
			!checkRelativePosition()) return 66; //bottom wall, no paddle.
	else if (ball.posY + ball.radius - 1 >= cvs.height - PADDLE.height &&
			checkRelativePosition()) return 4; //paddle reflection
	else if (ball.posY - ball.radius < 0) return 2;
	
	/// BLOCK COLISIONS ///
	else for(var j = ROWS_OF_BLOCKS-1; j >= 0; j--){
			for(var i = cols-1; i >= 0; i--)
			
			{ 	
				if(grid[i][j].hit) continue;
				var Corner = {	LT: {x: grid[i][j].x, y: grid[i][j].y},
								RT: {x: grid[i][j].x + grid[i][j].width, y: grid[i][j].y},
								RB: {x: grid[i][j].x + grid[i][j].width, y: grid[i][j].y + grid[i][j].height},
								LB: {x: grid[i][j].x, y: grid[i][j].y + grid[i][j].height}
				};
				var Sides = {B: grid[i][j].y + grid[i][j].height, T: grid[i][j].y, L: grid[i][j].x, R: grid[i][j].x + grid[i][j].width};
				Ball = {B: ball.posY - ball.radius, T: ball.posY + ball.radius, L: ball.posX + ball.radius, R: ball.posX - ball.radius};
				
				//Flat areas: top, bottom, right, left
				
				//top, bottom
				if(((Ball.B <= Sides.B) && grid[i][j].borders.B || (Ball.T >= Sides.T) && grid[i][j].borders.T) && (ball.posX >= Sides.L && ball.posX <= Sides.R)){
					grid[i][j].hit = true;
					return 2;
				}
				//right, left
				if((Ball.L >= Sides.L && grid[i][j].borders.L || Ball.R <= Sides.R && grid[i][j].borders.R) && (ball.posY >= Sides.T && ball.posY <= Sides.B)) {
					grid[i][j].hit = true;
					return 1;
				}
				//Corner areas: right-top, left-top, right-bottom, left-bottom
				
				//right-bottom
				if(ball.posX > Corner.RB.x && ball.posY > Corner.RB.y){
					if(Math.sqrt(Math.pow(ball.posX - Corner.RB.x, 2) + Math.pow(ball.posY - Corner.RB.y, 2)) < ball.radius) {
						var cosinus = (ball.posX - Corner.RB.x)/ball.radius;
						var sinus = Math.sin(Math.acos(Math.abs(cosinus)));
						ball.speedX = cosinus * MAX_SPEED;
						ball.speedY = sinus * MAX_SPEED;
						grid[i][j].hit = true;
						return 5;
					}
				}
				//right-top
				if(ball.posX > Corner.RT.x && ball.posY < Corner.RT.y){
					if(Math.sqrt(Math.pow(ball.posX - Corner.RT.x, 2) + Math.pow(ball.posY - Corner.RT.y, 2)) < ball.radius) {
						var cosinus = (ball.posX - Corner.RT.x)/ball.radius;
						var sinus = Math.sin(Math.acos(Math.abs(cosinus)));
						ball.speedX = cosinus * MAX_SPEED;
						ball.speedY = -sinus * MAX_SPEED;
						grid[i][j].hit = true;
						return 6;
					}
				}
				//left-top
				if(ball.posX < Corner.LT.x && ball.posY < Corner.LT.y){
					if(Math.sqrt(Math.pow(ball.posX - Corner.LT.x, 2) + Math.pow(ball.posY - Corner.LT.y, 2)) < ball.radius){
						var cosinus = (ball.posX - Corner.LT.x)/ball.radius;
						var sinus = Math.sin(Math.acos(Math.abs(cosinus)));
						ball.speedX = cosinus * MAX_SPEED;
						ball.speedY = -sinus * MAX_SPEED;
						grid[i][j].hit = true;
						return 7;
					}
				}
				//left-bottom
				if(ball.posX < Corner.LB.x && ball.posY > Corner.LB.y){
					if(Math.sqrt(Math.pow(ball.posX - Corner.LB.x, 2) + Math.pow(ball.posY - Corner.LB.y, 2)) < ball.radius){						
						var cosinus = (ball.posX - Corner.LB.x)/ball.radius;
						var sinus = Math.sin(Math.acos(Math.abs(cosinus)));
						ball.speedX = cosinus * MAX_SPEED;
						ball.speedY = sinus * MAX_SPEED;
						grid[i][j].hit = true;
						return 8;
					}
				}
			}
		}
	return -1;
}

function checkRelativePosition(){
	var left = PADDLE.posX - ball.posX - ball.radius;
	var right = PADDLE.posX + PADDLE.width - ball.posX + ball.radius;
	
	if(left > 0) return false;
	else if(right < 0) return false;
	else return true;
}

function checkReflectionAngle(){
	var cosinus = (ball.posX - PADDLE.posX - (PADDLE.width/2))/((PADDLE.width/2) + ball.radius);
	var sinus = Math.sin(Math.acos(Math.abs(cosinus)));
	var multiplayer = 1.4; //
	return {
		x: cosinus * multiplayer,
		y: sinus
	}
	
}

function checkBorders(){
	for(var i = 0; i < cols; i++){
		for(var j = 0; j < ROWS_OF_BLOCKS; j++)
		{
			var Sides = {B: grid[i][j].y + grid[i][j].height, T: grid[i][j].y, L: grid[i][j].x, R: grid[i][j].x + grid[i][j].width};
			if(ball.speedX <= 0 && Ball.L <= Sides.L)
			{
				grid[i][j].setBorders('L');
			}
			if(ball.speedX >= 0 && Ball.R >= Sides.R) 
			{
				grid[i][j].setBorders('R');
			}
			if(ball.speedY < 0 && Ball.T <= Sides.T) 
			{
				grid[i][j].setBorders('T');
			}
			if(ball.speedY > 0 && Ball.B >= Sides.B) {
				grid[i][j].setBorders('B');
			}
		}
	}
}

function checkOffsetCounter(){
	if(counter == GO_DOWN) {
		counter = 0;
		for(var i = 0; i < cols; i++){
		for(var j = 0; j < ROWS_OF_BLOCKS; j++){
			grid[i][j].setOffset(BLOCK.height);
			
		}
		}
	}
}

function checkLost(){
	for(var i = 0; i < cols; i++){
		for(var j = 0; j < ROWS_OF_BLOCKS; j++)
		{
			if(grid[i][j].hit) continue;
			if((grid[i][j].y + grid[i][j].height) > cvs.height) isLost = true;
		} 
	}
}

function checkKey(e) {
	
    e = e || window.event;
	
	//space
    if (e.keyCode == '32') {
		checkBorders();
		ballMoving = !ballMoving;
	}
  
}